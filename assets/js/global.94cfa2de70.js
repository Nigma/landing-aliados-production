/**
 *
 * @author      Jeison Sanchez <jeison@nigma.co>
 * @website     http://www.nigma.co
 * @copyright   2018 Jeison Sanchez
 *
 * @frontend-framework
 * Name        : Nigma Framework
 * Description : Código pre-horneado listo para mezclar, probar y disfrutar! ♥
 * Version     : 3.0.0
 * Author      : Jeison Sanchez <jeison@nigma.co>
 * Website     : http://www.nigma.co
 * License     : MIT
 *
 */
$(function(){function e(){var e=$("#formulario");e.isVisible()?0==n&&($(".js-btn-mensaje").animate({right:"-300px"},400),n=1):1==n&&($(".js-btn-mensaje").animate({right:"12px"},400),n=0)}Hyphenator.run();var t={stringsElement:"#typed-strings",typeSpeed:20,backSpeed:20,backDelay:2e3,loop:!0,fadeOut:!0,loopCount:1/0};new Typed("#typed",t);$(".o-form-field").focus(function(){$(this).parent(".c-form__item").addClass("is-icon-hover")}),$(".o-form-field").blur(function(){$(this).parent(".c-form__item").removeClass("is-icon-hover")}),$("#formContacto").validate({ignore:".ignore",rules:{nombre:"required",email:{required:!0,email:!0},celular:"required",mensaje:"required"},submitHandler:function(e){var t=$(".js-btn-contacto"),n=$("#formContacto").serialize(),i="/php/contacto/enviar-formu-contacto.php",o="https://lb.benchmarkemail.com//code/lbform";t.attr("disabled",!0),$(".js-notificacion").slideUp(400),$(".js-menu-mobile-overlay").fadeIn(200),$(".js-loader").fadeIn(400);var a=$("[name=nombre]").val(),r=$("[name=email]").val(),l=$("[name=celular]").val(),c=($("[name=mensaje]").val(),"mFcQnoBFKMQLXfarAJGcFsxpSu0%2BSv5KgP%2FL%2F5l5SE3tq3l2RqrLzw%3D%3D"),d="";$.post(i,n,function(e){t.removeAttr("disabled"),"1"==e.match("1")&&(t.removeAttr("disabled"),fbq("track","Lead"),$(".js-notificacion").slideDown(400),$('[class*="o-form-field"]').val(""),$("#check_politicas").removeAttr("checked"),$(".js-menu-mobile-overlay").fadeOut(200),$(".js-loader").fadeOut(400),$.post(o,{fldfirstname:a,fldEmail:r,fldfield8:l,token:c,doubleoptin:d},function(e){}))})},invalidHandler:function(){}}),$.fn.isVisible=function(){var e=this[0].getBoundingClientRect();return(e.height>0||e.width>0)&&e.bottom>=0&&e.right>=0&&e.top<=(window.innerHeight||document.documentElement.clientHeight)&&e.left<=(window.innerWidth||document.documentElement.clientWidth)};var n=1;$(window).scroll(function(){e()}),$.scrollIt({topOffset:-60})});