<?php


/*	^Aplicar solo 2 decimales a un numero
-------------------------------------------------------------*/

function decimal($numero){

		 return number_format($numero,2,".","");

}
/*	^Fin - Aplicar solo 2 decimales a un numero
-------------------------------------------------------------*/


/**
 * Payu Class
 */
class Payu
{

	public $apiKey;
	public $usuarioID;
	public $merchantID;
	public $prueba = 0;
	public $firma;
	public $refVenta;

	public $precio;
	public $valor;
	public $iva;
	public $sinIVA;
	public $moneda = "COP";
	public $descripcionCompra;

	public function config(){

		$ApiKey = $this->apiKey; //llave de encripción que se usa para generar la fima
		$usuarioId = $this->usuarioID; //código único del cliente
		$merchantId = $this->merchantID; //código único del cliente
		$prueba = $this->prueba; //variable para poder utilizar tarjetas de crédito de prueba


		// Impuesto
		$impuesto_iva = 19;
		$total_sin_iva = $this->precio / 1.19;
		$total_iva = ($total_sin_iva * $impuesto_iva) / 100;



		$_SESSION["orden_id"] = "NB" . date('YmdHis');


		$this->refVenta = ((isset($_SESSION["orden_id"]))?$_SESSION["orden_id"]:""); //referencia que debe ser única para cada transacción
		$this->iva = decimal($total_iva); //impuestos calculados de la transacción
		$this->sinIVA = decimal($total_sin_iva); //el precio sin iva de los productos que tienen iva
		$this->valor = decimal($this->precio); //el valor total

		// $nombre_comprador = $datosUsuario->getColumnVal("nombre") . " " . $datosUsuario->getColumnVal("apellido");
		$descripcion = $this->descripcionCompra; //descripción de la transacción
		// $url_respuesta = "";
		// $url_confirmacion = "";
		// $emailComprador = $datosUsuario->getColumnVal("correo"); //email al que llega confirmación del estado final de la transacción
		$firma_cadena = "$this->apiKey~$this->merchantID~$this->refVenta~$this->valor~$this->moneda"; //concatenación para realizar la firma
		$this->firma = md5($firma_cadena); //creación de la firma con la cadena previamente hecha

	}

}


// $venta = new Payu();
// $venta->apiKey = "4Vj8eK4rloUd272L48hsrarnUA";
// $venta->usuarioID = 512321;
// $venta->merchantID = 508029;
// $venta->prueba = 1;
// $venta->precio = 54000;
// $venta->descripcionCompra = "6º Congreso Nacional de Actualización Tributaria";
// $venta->config();


// echo decimal(50000);

?>
