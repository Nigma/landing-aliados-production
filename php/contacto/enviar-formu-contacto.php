<?php require_once('../funciones-global.php'); ?>
<?php

// Purificar HTML
require_once '../htmlpurifier/library/HTMLPurifier.auto.php';
$config = HTMLPurifier_Config::createDefault();
$config->set('HTML.Allowed', '');
$purifier = new HTMLPurifier($config);
// Fin - Purificar HTML


$datos = array(

	"nombre" => ucwords(strtolower($purifier->purify(((isset($_POST["nombre"]))?$_POST["nombre"]:"")))),
	"email" => strtolower($purifier->purify(((isset($_POST["email"]))?$_POST["email"]:""))),
	"celular" => $purifier->purify(((isset($_POST["celular"]))?$_POST["celular"]:"")),
	"mensaje" => $purifier->purify(((isset($_POST["mensaje"]))?$_POST["mensaje"]:"")),

);

  	// Enviar correo
	ob_start();
	if (!session_id()) session_start();
	function plantilla($fileName, $datos)  {
		$Include_Start_Dir = getcwd();
		$Include_Dir =  dirname($fileName);
		chdir(dirname(__FILE__));
		chdir($Include_Dir);
		ob_start();
		require(basename($fileName));
		$content = ob_get_contents();
		ob_end_clean();
		chdir($Include_Start_Dir);
		return $content;
	}


	// Enviar email para activar cuenta
	require '../Mailin.php';


	$mailin = new Mailin('https://api.sendinblue.com/v2.0','1DSvcrsPkhpJMKHY');

	$data = array(
			"to" => array(
				"jeison@nigma.co"=>"Jeison Sánchez",
				"contacto@aliados-sii.com"=>"Aliados Sii"
			),
			"from" => array(SMTP_FROM, SMTP_FROM_NAME),
			"replyto" => array($datos["email"], $datos["nombre"]),
			"subject" => "Contacto 6º Congreso Nacional de Actualización Tributaria - Ref: " . date("mds"),
			"html" => plantilla("plantilla-correo-contacto.php", $datos),
			"headers" => array("Content-Type"=> "text/html; charset=UTF-8")
	);

	$mailin->send_email($data);

	// devuelvo 1 para indicar que el registro se hizo exitoso
	echo 1;

?>
