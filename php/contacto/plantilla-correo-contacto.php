<?php
if (!session_id()) session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<title>Respuesta correo</title>
<style type="text/css">
.pepe {
	-webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px;
}
.pepe2 {

}
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" align="center" bgcolor="#f0f0f0" style="padding-left: 0px; padding-bottom: 40px; padding-right: 0px; padding-top: 0px; margin: 0px;">
    <table width="576" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="middle" style="padding-bottom: 10px; padding-top: 20px;">&nbsp;</td>
        </tr>
    </table>
    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="-webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px; background-color: #FFF; border: 1px solid #d1d1d1; -webkit-box-shadow: 0px 0px 12px #dbdbdb; -moz-box-shadow: 0px 0px 12px #dbdbdb; box-shadow: 0px 0px 12px #dbdbdb;">
      <tr>
        <td width="100%" height="44" align="center" valign="middle" bgcolor="#53AAE8" style="-webkit-border-radius: 10px 10px 0px 0px; -moz-border-radius: 10px 10px 0px 0px; border-radius: 10px 10px 0px 0px; border-bottom: 3px solid #53AAE8;"><table width="564" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" style="font-size: 18px; color: #FFF; font-family: Arial, Helvetica, sans-serif;">Hola!</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF" style="padding-left: 0px; padding-bottom: 20px; padding-right: 0px; padding-top: 24px; margin: 0px; -webkit-border-radius: 0px 0px 10px 10px; -moz-border-radius: 0px 0px 10px 10px; border-radius: 0px 0px 10px 10px;"><table width="564" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>

            <td align="left" style="margin-left: 0px; margin-bottom: 18px; margin-right: 0px; margin-top: 0px; padding: 0px; line-height: 20px; color: #545353; font-size: 15px; font-family: Arial, Helvetica, sans-serif;">
            	<p style="margin-left: 0px; margin-bottom: 18px; margin-right: 0px; margin-top: 0px; padding: 0px; line-height: 20px; color: #545353; font-size: 15px; font-family: Arial, Helvetica, sans-serif;">Un persona ha enviado un mensaje desde el formulario de contacto:</p>

                <p style="margin-left: 0px; margin-bottom: 18px; margin-right: 0px; margin-top: 0px; padding: 0px; line-height: 20px; color: #545353; font-size: 15px; font-family: Arial, Helvetica, sans-serif;">

                  <strong style="color:#111;">Nombre:</strong> <?php echo $datos["nombre"]; ?><br />
                  <strong style="color:#111;">Correo electrónico:</strong> <?php echo $datos["email"]; ?><br />
                  <strong style="color:#111;">Celular:</strong> <?php echo $datos["celular"]; ?><br />
                  <br />
                  <strong style="color:#111;">Mensaje:</strong> <?php echo $datos["mensaje"]; ?><br />
                </p>

                </td>
          </tr>
        </table></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
