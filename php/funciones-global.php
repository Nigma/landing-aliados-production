<?php

/*	^Limpiar caracteres
-------------------------------------------------------------*/

function limpiar_caracteres($cadena) {
		$cadena = trim($cadena);
		$cadena = strtr($cadena,
	"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
	"aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
		$cadena = strtr($cadena,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz");
		$cadena = preg_replace('#([^a-z0-9]+)#i', '-', $cadena);
		$cadena = preg_replace('#-{2,}#','-',$cadena);
		$cadena = preg_replace('#-$#','',$cadena);
		$cadena = preg_replace('#^-#','',$cadena);
		$cadena = preg_replace('#-jpg#','',$cadena);
		$cadena = preg_replace('#-jpeg#','',$cadena);
		$cadena = preg_replace('#-png#','',$cadena);
		$cadena = preg_replace('#-gif#','',$cadena);
		$cadena = preg_replace('#-doc#','',$cadena);
		$cadena = preg_replace('#-xls#','',$cadena);
		$cadena = preg_replace('#-xlsx#','',$cadena);
		$cadena = preg_replace('#-pdf#','',$cadena);
		$cadena = preg_replace('#-ai#','',$cadena);
		$cadena = preg_replace('#-psd#','',$cadena);
		$cadena = preg_replace('#-pdf#','',$cadena);
		$cadena = preg_replace('#-txt#','',$cadena);
		$cadena = preg_replace('#-ppt#','',$cadena);
		$cadena = preg_replace('#-pptx#','',$cadena);
		$cadena = preg_replace('#-zip#','',$cadena);
		$cadena = preg_replace('#-mp4#','',$cadena);
		return $cadena;
}
/*	^Fin - Limpiar caracteres
-------------------------------------------------------------*/





/*	^Slug URL SEO
-------------------------------------------------------------*/

function slugify($string, $replace = array(), $delimiter = '-') {
  // https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
  if (!extension_loaded('iconv')) {
    throw new Exception('iconv module not loaded');
  }
  // Save the old locale and set the new locale to UTF-8
  $oldLocale = setlocale(LC_ALL, '0');
  setlocale(LC_ALL, 'en_US.UTF-8');
  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
  if (!empty($replace)) {
    $clean = str_replace((array) $replace, ' ', $clean);
  }
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower($clean);
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
  $clean = trim($clean, $delimiter);
  // Revert back to the old locale
  setlocale(LC_ALL, $oldLocale);
  return $clean;
}

/*	^Fin - Slug URL SEO
-------------------------------------------------------------*/





/*	^Dividir nombre para extraer nombre, segundo nombre
	o apellido
-------------------------------------------------------------*/

function split_name($name) {
    $parts = array();

    while ( strlen( trim($name)) > 0 ) {
        $name = trim($name);
        $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $parts[] = $string;
        $name = trim( preg_replace('#'.$string.'#', '', $name ) );
    }

    if (empty($parts)) {
        return false;
    }

    $parts = array_reverse($parts);
    $name = array();
    $name['primer_nombre'] = $parts[0];
    $name['segundo_nombre'] = (isset($parts[1])) ? $parts[1] : '';
    $name['primer_apellido'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');
	$name['segundo_apellido'] = (isset($parts[3])) ? $parts[3] : '';

    return $name;
}

/*	^Fin - Dividir nombre para extraer nombre, segundo nombre
	o apellido
-------------------------------------------------------------*/



/*	^Agregar clase active al menu
-------------------------------------------------------------*/

	function menuActive2( $url, $class=false ) {

		$archivo_actual = basename($_SERVER['PHP_SELF']); //muestra el nombre del archivo actual

		if(($archivo_actual == $url) && ($class == false)){
			return "current-menu-item";
		}
		else if(($archivo_actual == $url) && ($class != false)){
			return $class;
		}

	}

/*	^Fin - Agregar clase active al menu
-------------------------------------------------------------*/






/*	^Agregar clase active al menu
-------------------------------------------------------------*/

	function menuActive($id, $ref, $id2){

			 if($id == $ref){
				 echo 'active';
				}
			 elseif($id == $id2){
				 if($ref == ""){
					echo 'active';
				 }
			 }
	}

/*	^Fin - Agregar clase active al menu
-------------------------------------------------------------*/







/*	^Mostrar solo dia de mes
-------------------------------------------------------------*/

	function diaMes( $fecha ) {

		return date("j", strtotime("$fecha") );
	}

/*	^Fin - Mostrar solo dia de mes
-------------------------------------------------------------*/


/*	^Mostrar solo mes
-------------------------------------------------------------*/

	function mes( $fecha, $idioma="") {

		if($idioma == "es"){

			setlocale(LC_ALL,"es_ES","es_ES","esp"); // Define idioma español para la fecha
			date_default_timezone_set('America/Bogota'); /* Define Hora local Colombia */

		}


		//return date("j", strtotime("$fecha") );
		return strftime("%B", strtotime("$fecha") );
	}

/*	^Fin - Mostrar solo mes
-------------------------------------------------------------*/




/*	^Mostrar solo año
-------------------------------------------------------------*/

	function ano( $fecha, $idioma) {

		if($idioma == "es"){

			setlocale(LC_ALL,"es_ES","es_ES","esp"); // Define idioma español para la fecha
			date_default_timezone_set('America/Bogota'); /* Define Hora local Colombia */

		}
		//return date("j", strtotime("$fecha") );
		return strftime("%Y", strtotime("$fecha") );
	}

/*	^Fin - Mostrar solo año
-------------------------------------------------------------*/





/*	^Convertir fecha para Livestamp.js
-------------------------------------------------------------*/

	function fechaPublicacion( $fecha ) {

		// $fecha = "2016-08-01 02:55:07";
		return date("Y-m-d\TH:i:s-05:00", strtotime($fecha) );
	}

/*	^Fin - Convertir fecha para Livestamp.js
-------------------------------------------------------------*/



/*	^Datos evento
-------------------------------------------------------------*/

	function datoEvento( $dato, $vacio ) {

		if($dato != ""){
			return $dato;
		} else {
			return $vacio;
		}
	}

/*	^Fin - Datos evento
-------------------------------------------------------------*/




/*	^Recortar texto
-------------------------------------------------------------*/

	function recortar_texto($texto, $limite_caracteres)
	{
	 if(substr($texto,$limite_caracteres-1,1) != ' ')
		{
		   $texto = substr($texto,'0',$limite_caracteres);
		   $array = explode(' ',$texto);
		   array_pop($array);
		   $new_texto = implode(' ',$array);

		  return $new_texto.' ...';
		}
		  else {
				 return substr($texto,'0',$limite_caracteres-1).' ...';
			   }
	}


/*	^Fin - Recortar texto
-------------------------------------------------------------*/




/*	^Definir constantes
-------------------------------------------------------------*/

	define("SMTP_HOST", "smtp-relay.sendinblue.com");
	define("SMTP_USERNAME", "jeison@nigma.co");
	define("SMTP_PASSWORD", "1DSvcrsPkhpJMKHY");
	define("SMTP_FROM", "noreply@contactenosya.com");
	define("SMTP_FROM_NAME", "Contacto Web");

/*	^Fin - Definir constantes
-------------------------------------------------------------*/
?>
