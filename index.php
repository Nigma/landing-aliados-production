<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no, email=no, address=no, date=no">
    <title>6º Congreso Nacional de Actualización Tributaria</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<!-- og Facebook -->
	<meta property="og:title" content="6º Congreso Nacional de Actualización Tributaria">
	<meta property="og:description" content="¿Qué ha pasado después de la reforma? - Principales cambios en: * Renta * IVA * Retefuente * Otros Impuestos - La nueva Revisoría Fiscal - Investigación de la evasión - Conciliaciones fiscales - Auditoría forense - Medios tecnológicos.">
	<meta property="og:image" content="https://congresovirtual.aliados-sii.com/images/congreso-aliados-facebook.png">
	<!-- Fin - og Facebook -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,500,700|Roboto:400,500,700">
    <link rel="stylesheet" href="assets/css/global.9ab6fdc685.css">

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '377313086026835');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=377313086026835&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

</head>

<body>
    <div class="c-block-1">
        <div class="g-container c-block-1__container">
            <header class="c-header">
                <a class="c-header__logo" href="/">
                    <img src="/images/logo-aliados.png" alt="Logo Aliados Sii">
                </a>
                <div class="c-header__contact">
                    <div class="o-text-icon icon-envelop">contacto@aliados-sii.com</div>
                    <div class="o-text-icon icon-phone-wave">316 2344902 - 311 4543029</div>
                </div>
            </header>
            <!-- /.c-header-->
            <div class="c-header-2">
                <div class="c-header-2__box">
                    <div class="c-header-2__number">6</div>
                    <div class="c-header__box-2">
                        <h1 class="c-header-2__title">Congreso Nacional <span>de Actualización Tributaria</span></h1>
                        <div class="c-header-2__box-3">
                            <div class="c-header-2__date">Febrero 1 y 2 de 2018</div>
                            <div class="c-header-2__modality">Modalidad: Virtual en vivo</div>
                        </div>
                    </div>
                </div>
                <div class="c-header-2__subtitle">
                    <div id="typed-strings">
                        <p>Inteligencia Tributaria 2018</p>
                        <p>¿Qué ha pasado después de la reforma?</p>
                        <p>Cambios en Renta, IVA, Retefuente y otros impuestos</p>
                        <p>La nueva Revisoría Fiscal</p>
                        <p>Conciliaciones fiscales</p>
                        <p>Cómo aplicar Codigo de etica, calidad y NIAS</p>
                        <p>Investigación de la evasión</p>
                        <p>Auditoría forense</p>
                        <p>Medios tecnológicos</p>
                    </div><span id="typed"></span>
                </div>
            </div>
            <div class="c-header-3">
                <div class="c-header-3__box">
                    <div class="c-header-3__price">
                        <div class="c-header-3__box-2">
                            <div class="c-header-3__price-text">Hoy solo <span>pagas</span>
                            </div>
                            <div class="c-header-3__price-number">$447.000</div>
                        </div>
                        <div class="c-header-3__box-3">
                            <div class="c-header-3__despues">Después <span>$597.000</span>
                            </div>
                        </div>
                    </div>
                    <div class="c-header-3__count">
                        <div class="c-header-3__title-count" style="display:none;">Descuento hasta el 26 de Enero</div>
                        <div class="contador">
                            <div class="soon" id="contador" data-due="2018-01-31T23:59:00-05:00" data-layout="group label-uppercase label-small" data-scale-max="fill" data-format="d,h,m,s" data-separator=":" data-face="slot slide" data-labels-days="Días,Días"
                            data-labels-hours="Horas,Horas" data-labels-minutes="Minutos,Minutos" data-labels-seconds="Seg,Seg"></div>
                        </div>
                    </div>
                </div>
				<form class="c-header-3__btn">
                    <a href="/procesar-pago.php" class="o-btn-buy"> <span><i class="icon-price-tag"></i> COMPRAR ENTRADA</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
    <div class="c-block-2">
        <div class="g-container">
            <div class="c-block-2__heading">
                <h2 class="o-heading-large">¿Por qué inteligencia tributaría?</h2>
            </div>
            <p class="c-block-2__description">Porque los asesores, contadores, auditores y revisores debemos estar atentos y con herramientas inteligentes para cumplir nuestro objetivo profesional</p>
        </div>
    </div>
    <div class="c-block-3">
        <div class="g-container">
            <div class="c-list-1">
                <div class="c-list-1__item">
                    <img src="/images/img1.jpg" alt="Qué ha pasado después de la reforma">
                    <div class="c-list-1__box">
                        <h3 class="c-list-1__heading o-heading-normal-2">¿Qué ha pasado después de la reforma?</h3>
                        <p class="c-list-1__text">Principales cambios en: * Renta * IVA * Retefuente * Otros Impuestos</p>
                    </div>
                </div>
                <div class="c-list-1__item">
                    <img src="/images/img2.jpg" alt="Cómo las NIIF están afectando los impuestos">
                    <div class="c-list-1__box">
                        <h3 class="c-list-1__heading o-heading-normal-2">¿Cómo las NIIF están afectando los impuestos? </h3>
                        <p class="c-list-1__text">Conciliaciones fiscales, Impuestos Diferidos entre otras afectaciones</p>
                    </div>
                </div>
                <div class="c-list-1__item">
                    <img src="/images/img3.jpg" alt="Qué analizaremos sobre el aseguramiento?">
                    <div class="c-list-1__box">
                        <h3 class="c-list-1__heading o-heading-normal-2">¿Qué analizaremos sobre el aseguramiento? </h3>
                        <p class="c-list-1__text">La nueva Revisoría Fiscal, auditorias y cómo aplicar Codigo de etica, calidad y NIAS</p>
                    </div>
                </div>
                <div class="c-list-1__item">
                    <img src="/images/img4.jpg" alt="La inteligencia tributaría frente a la evasión">
                    <div class="c-list-1__box">
                        <h3 class="c-list-1__heading o-heading-normal-2">La inteligencia tributaría frente a la evasión</h3>
                        <p class="c-list-1__text">Investigación de la evasión, auditoría forense, cruces de información, medios tecnológicos</p>
                    </div>
                </div>
                <div class="c-list-1__item">
                    <img src="/images/img5.jpg" alt="Beneficios, sanciones y procedimiento tributarío">
                    <div class="c-list-1__box">
                        <h3 class="c-list-1__heading o-heading-normal-2">Beneficios, sanciones y procedimiento tributarío</h3>
                        <p class="c-list-1__text">Resúmen del régimen sancionatorio, principales beneficios que aún existen</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-block-4">
        <div class="g-container c-block-1__container">
            <div class="c-block-2__heading--v2">
                <h2 class="o-heading-large">Expertos Conferencistas</h2>
            </div>
            <p class="c-block-2__description--v2">Aquí están los conferencistas, a lo largo de estas cinco versiones han salido en hombros cada vez que nos comparten su conocimiento y experiencia</p>
            <div class="c-list-2">
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img1.jpg" alt="Luis Fernando García Caicedo">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Luis Fernando García Caicedo</div>
                        <p class="c-list-2__text">Magister en Contabilidad Forense y Criminología</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img2.jpg" alt="Jaime Monclou Pedraza">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Jaime Monclou Pedraza</div>
                        <p class="c-list-2__text">Experto en Renta y las ESAL</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img3.jpg" alt="Eleycer Camargo Marin">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Eleycer Camargo Marin</div>
                        <p class="c-list-2__text">Experto en Retefuente</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img4.jpg" alt="Jose Elbert Castañeda">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Jose Elbert Castañeda</div>
                        <p class="c-list-2__text">Experto en IVA</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img5.jpg" alt="Jorge Enrique Beltran T.">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Jorge Enrique Beltran T.</div>
                        <p class="c-list-2__text">Experto Información Exógena</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img6.jpg" alt="Oscar Horacio Torres G.">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Oscar Horacio Torres G.</div>
                        <p class="c-list-2__text">Revisor fiscal experto en NIIF y NIAS</p>
                    </div>
                </div>
                <div class="c-list-2__item">
                    <div class="c-list-2__col-1">
                        <img class="c-list-2__img" src="/images/conferencistas/img7.jpg" alt="Juan Carlos Leyton Díaz">
                    </div>
                    <div class="c-list-2__col-2">
                        <div class="c-list-2__heading o-heading-normal-3">Juan Carlos Leyton Díaz</div>
                        <p class="c-list-2__text">Experto Auditoría Externa, NIIF y NIAS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-block-4__box c-block-1__container">
            <div class="g-container">
                <h3 class="c-block-4__heading">Compra antes de que finalice el descuento</h3>
                <div class="c-block-4__contador">
                    <div class="contador">
                        <div class="soon" id="contador-2" data-due="2018-01-31T23:59:00-05:00" data-layout="group label-uppercase label-small" data-scale-max="fill" data-format="d,h,m,s" data-separator=":" data-face="slot slide" data-labels-days="Días,Días"
                        data-labels-hours="Horas,Horas" data-labels-minutes="Minutos,Minutos" data-labels-seconds="Seg,Seg"></div>
                    </div>
                </div>
				<form class="c-header-3__btn">
                    <a href="/procesar-pago.php" class="o-btn-buy"> <span><i class="icon-price-tag"></i> COMPRAR ENTRADA</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
    <div class="c-block-2" id="formulario" data-scroll-index="0">
        <div class="g-container">
            <div class="c-block-2__heading">
                <h2 class="o-heading-large">¿Deseas más información?</h2>
            </div>
            <p class="c-block-2__description">Déjanos tus datos y te contactamos</p>
            <form class="c-form-full" id="formContacto" name="form-contacto">
                <div class="c-form-1 l-form-1">
                    <div class="l-form-1__col">
                        <div class="c-form-1__input">
                            <input class="o-form-field" type="text" name="nombre" placeholder="Nombre">
                        </div>
                        <div class="c-form-1__input">
                            <input class="o-form-field" type="text" name="email" placeholder="Correo electrónico">
                        </div>
                        <div class="c-form-1__input">
                            <input class="o-form-field" type="text" name="celular" placeholder="Celular">
                        </div>
                    </div>
                    <div class="c-form-1__col l-form-1__col">
                        <textarea class="c-form-1__textarea o-form-field--textarea" name="mensaje" placeholder="Mensaje"></textarea>
                    </div>
                    <div class="l-form-1__col-full--center is-hidden js-notificacion">
                        <div class="notificacion--ok"><span class="icon-checkmark"></span>  <span class="mensaje">Mensaje enviado exitosamente, nos pondremos en contacto muy pronto.</span>
                        </div>
                    </div>
                    <div class="l-form-1__col-full--center">
                        <button class="o-btn js-btn-contacto">ENVIAR</button>
                    </div>
                </div>
            </form>
            <div class="c-block-2__box">
                <p>Organiza</p>
                <img class="organiza" src="/images/logo-aliados-2.png" alt="Logo Aliados v2"><a class="nigma" href="https://www.nigma.co/" target="_blank"><span>Web Design</span><img src="/images/logo-nigma.svg" alt="Logo Nigma"></a>
            </div>
        </div>
    </div>
    <div class="menu-mobile-overlay js-menu-mobile-overlay"></div>
    <div class="loader-popup js-loader">
        <div class="loader__box">
            <div class="icon-spinner9 animate-spin"></div>
        </div>
    </div>
	<div class="o-btn--v3 js-btn-mensaje" data-scroll-nav="0"><i class="icon-envelop5"></i> Enviar mensaje</div>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111609220-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-111609220-1');
	</script>


    <!-- jQuery 1.11.2 -->
    <script src="/assets/js/vendor.22d612972f.js"></script>
    <!-- Scripts globales -->
    <script src="/assets/js/global.94cfa2de70.js"></script>
</body>

</html>
