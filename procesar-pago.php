<?php
require_once "php/payu-class.php";

$venta = new Payu();
$venta->apiKey = "11952b350d3";
$venta->usuarioID = 26187;
$venta->merchantID = 22129;
$venta->prueba = 0;
$venta->precio = 397000;
$venta->descripcionCompra = "6º Congreso Nacional de Actualización Tributaria - Modalidad: Virtual en vivo";
$venta->config();

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Procesando Pago</title>

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '377313086026835');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=377313086026835&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

</head>
<body>

	<form id="pagar" class="c-header-3__btn" method="post" action="https://gateway.payulatam.com/ppp-web-gateway/">
		<input name="merchantId" type="hidden" value="<?php echo $venta->merchantID; ?>">
		<input name="accountId" type="hidden" value="<?php echo $venta->usuarioID; ?>">
		<input name="description" type="hidden" value="<?php echo $venta->descripcionCompra; ?>">

		<input name="referenceCode" type="hidden" value="<?php echo $venta->refVenta; ?>">
		<input name="amount" type="hidden" value="<?php echo $venta->valor; ?>">

		<input name="tax" type="hidden" value="<?php echo $venta->iva; ?>">

		<input name="taxReturnBase" type="hidden" value="<?php echo $venta->sinIVA; ?>">

		<input name="currency" type="hidden" value="<?php echo $venta->moneda; ?>">
		<input name="lng" type="hidden" value="es">
		<input name="test" type="hidden" value="<?php echo $venta->prueba; ?>">

		<!-- <input name="responseUrl" id="urlOrigen" value="" type="hidden">
		<input name="confirmationUrl" id="urlOrigen" value="" type="hidden"> -->

		<input name="signature" value="<?php echo $venta->firma; ?>" type="hidden">

	</form>


	<!-- jQuery 1.11.2 -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script>
    	$(function(){
			fbq('track', 'InitiateCheckout');

			setTimeout(function(){
				$('#pagar').submit();
			}, 1000);

		});
    </script>

</body>
</html>
